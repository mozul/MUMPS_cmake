# include of mumps
include_directories(${CMAKE_SOURCE_DIR}/include)
# include of mpi (sequential of real one)
include_directories(${MPI_Fortran_INCLUDE_PATH})

# copying input files
file(COPY input_simpletest_real  DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
file(COPY input_simpletest_cmplx DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

# simple C driver (only available for dmumps)
if(${ENABLE_REAL} AND ${ENABLE_DOUBLE_PRECISION})
  add_executable(c_example c_example.c)
  target_link_libraries(c_example dmumps)
  add_test(c_example c_example)
endif(${ENABLE_REAL} AND ${ENABLE_DOUBLE_PRECISION})

# simple Fortran driver:
#   this sould not work on WINDOWS:
#   it would be easier for CTest if executable would read input
#   file instead of using shell redirection
foreach(ARITH ${MUMPS_BUILD_VERSIONS})

  add_executable(${ARITH}simpletest ${ARITH}simpletest.F)
  target_link_libraries(${ARITH}simpletest ${ARITH}mumps)

  if( ${ARITH} STREQUAL "d" OR ${ARITH} STREQUAL "s" )
    add_test(NAME ${ARITH}simpletest
             COMMAND sh -c "$<TARGET_FILE:${ARITH}simpletest> < input_simpletest_real"
            )
  else( ${ARITH} STREQUAL "d" OR ${ARITH} STREQUAL "s" )
    add_test(NAME ${ARITH}simpletest
             COMMAND sh -c "$<TARGET_FILE:${ARITH}simpletest> < input_simpletest_cmplx"
            )
  endif( ${ARITH} STREQUAL "d" OR ${ARITH} STREQUAL "s" )

endforeach(ARITH)
