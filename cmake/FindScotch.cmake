# set variables
set(Scotch_FOUND FALSE)
set(Scotch_LIBRARIES)
set(Scotch_INCLUDE_DIR)

message(STATUS "Searching for Scotch library ...")
# check if SCOTCH_ROOT is set
if(NOT SCOTCH_ROOT AND NOT $ENV{SCOTCH_ROOT} STREQUAL "")
  SET(SCOTCH_ROOT $ENV{SCOTCH_ROOT})
endif(NOT SCOTCH_ROOT AND NOT $ENV{SCOTCH_ROOT} STREQUAL "")

# convert path to unix style path and set search path
if(SCOTCH_ROOT)
  file(TO_CMAKE_PATH ${SCOTCH_ROOT} SCOTCH_ROOT)
  set(_scotch_INCLUDE_SEARCH_DIRS ${SCOTCH_ROOT}/include ${SCOTCH_ROOT} ${_scotch_INCLUDE_SEARCH_DIRS})
  set(_scotch_LIBRARIES_SEARCH_DIRS ${SCOTCH_ROOT}/lib ${SCOTCH_ROOT} ${_scotch_LIBRARIES_SEARCH_DIRS})
endif(SCOTCH_ROOT)

# search for header scotch.h
find_path(Scotch_INCLUDE_DIR NAMES scotch.h HINTS ${_scotch_INCLUDE_SEARCH_DIRS})
if(Scotch_INCLUDE_DIR)

  find_library(Scotch_LIB_ESMUMPS NAMES esmumps HINTS ${_scotch_LIBRARIES_SEARCH_DIRS})
  find_library(Scotch_LIB_SCOTCH NAMES scotch HINTS ${_scotch_LIBRARIES_SEARCH_DIRS})
  find_library(Scotch_LIB_SCOTCHERR NAMES scotcherr HINTS ${_scotch_LIBRARIES_SEARCH_DIRS})

  if(Scotch_LIB_ESMUMPS AND Scotch_LIB_SCOTCH AND Scotch_LIB_SCOTCHERR)
    message(STATUS "Scotch library found")
    set(Scotch_FOUND TRUE)
    set(Scotch_LIBRARIES ${Scotch_LIB_ESMUMPS} ${Scotch_LIB_SCOTCH} ${Scotch_LIB_SCOTCHERR})
  endif(Scotch_LIB_ESMUMPS AND Scotch_LIB_SCOTCH AND Scotch_LIB_SCOTCHERR)
endif(Scotch_INCLUDE_DIR)

# handle the QUIETLY and REQUIRED arguments
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Scotch DEFAULT_MSG MUMPSSolver_LIBRARIES Scotch_LIBRARIES)
