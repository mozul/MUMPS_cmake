# set variables
set(Metis_FOUND FALSE)
set(Metis_LIBRARIES)
set(Metis_INCLUDE_DIR)

message(STATUS "Searching for Metis library ...")
# check if MUMPS_ROOT is set
if(NOT METIS_ROOT AND NOT $ENV{METIS_ROOT} STREQUAL "")
  set(METIS_ROOT $ENV{METIS_ROOT})
endif(NOT METIS_ROOT AND NOT $ENV{METIS_ROOT} STREQUAL "")

# convert path to unix style path and set search path
if(METIS_ROOT)
  file(TO_CMAKE_PATH ${METIS_ROOT} METIS_ROOT)
  set(_metis_INCLUDE_SEARCH_DIRS ${METIS_ROOT}/include ${METIS_ROOT} ${_metis_INCLUDE_SEARCH_DIRS})
  set(_metis_LIBRARIES_SEARCH_DIRS ${METIS_ROOT}/lib ${METIS_ROOT} ${_metis_LIBRARIES_SEARCH_DIRS})
endif(METIS_ROOT)

# search for header metis.h
find_path(Metis_INCLUDE_DIR NAMES metis.h PATH_SUFFIXES metis HINTS ${_metis_INCLUDE_SEARCH_DIRS})

if(Metis_INCLUDE_DIR)

  #IF(UNIX AND Metis_FIND_STATIC_LIBRARY)
  #   SET(Metis_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES})
  #   SET(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
  #ENDIF(UNIX AND Metis_FIND_STATIC_LIBRARY)

  find_library(Metis_LIBRARIES NAMES metis HINTS ${_metis_LIBRARIES_SEARCH_DIRS})
  if(Metis_LIBRARIES)
    message(STATUS "Metis library found")
    set(Metis_FOUND TRUE)
  endif(Metis_LIBRARIES)

  #IF(UNIX AND Metis_FIND_STATIC_LIBRARY)
  #   SET(CMAKE_FIND_LIBRARY_SUFFIXES ${Metis_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES})
  #ENDIF(UNIX AND Metis_FIND_STATIC_LIBRARY)

endif(Metis_INCLUDE_DIR)

# handle the QUIETLY and REQUIRED arguments
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Metis DEFAULT_MSG Metis_LIBRARIES Metis_INCLUDE_DIR)
