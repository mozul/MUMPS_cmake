# set variables
set(ParMetis_FOUND FALSE)
set(ParMetis_LIBRARIES)
set(ParMetis_INCLUDE_DIR)

message(STATUS "Searching for ParMetis library ...")
# check if MUMPS_ROOT is set
if(NOT METIS_ROOT AND NOT $ENV{METIS_ROOT} STREQUAL "")
  set(METIS_ROOT $ENV{METIS_ROOT})
endif(NOT METIS_ROOT AND NOT $ENV{METIS_ROOT} STREQUAL "")

# convert path to unix style path and set search path
if(METIS_ROOT)
  file(TO_CMAKE_PATH ${METIS_ROOT} METIS_ROOT)
  set(_parmetis_INCLUDE_SEARCH_DIRS ${METIS_ROOT}/include ${METIS_ROOT} ${_parmetis_INCLUDE_SEARCH_DIRS})
  set(_parmetis_LIBRARIES_SEARCH_DIRS ${METIS_ROOT}/lib ${METIS_ROOT} ${_parmetis_LIBRARIES_SEARCH_DIRS})
endif(METIS_ROOT)

# search for header parmetis.h
find_path(ParMetis_INCLUDE_DIR NAMES parmetis.h PATH_SUFFIXES parmetis HINTS ${_parmetis_INCLUDE_SEARCH_DIRS})

if(ParMetis_INCLUDE_DIR)

  #IF(UNIX AND ParMetis_FIND_STATIC_LIBRARY)
  #   SET(ParMetis_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES})
  #   SET(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
  #ENDIF(UNIX AND ParMetis_FIND_STATIC_LIBRARY)

  find_library(ParMetis_LIBRARIES NAMES parmetis HINTS ${_parmetis_LIBRARIES_SEARCH_DIRS})
  if(ParMetis_LIBRARIES)
    message(STATUS "ParMetis library found")
    set(ParMetis_FOUND TRUE)
  endif(ParMetis_LIBRARIES)

  #IF(UNIX AND ParMetis_FIND_STATIC_LIBRARY)
  #   SET(CMAKE_FIND_LIBRARY_SUFFIXES ${ParMetis_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES})
  #ENDIF(UNIX AND ParMetis_FIND_STATIC_LIBRARY)

endif(ParMetis_INCLUDE_DIR)

# handle the QUIETLY and REQUIRED arguments
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ParMetis DEFAULT_MSG ParMetis_LIBRARIES ParMetis_INCLUDE_DIR)

