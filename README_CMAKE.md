# CMake files to build MUMPs library #

## About ##

This project is meant to help building [MUMPs library](http://mumps.enseeiht.fr/)
This project is mainly copied and adapted from two different sources:
* https://github.com/dariomangoni/MumpsWin
* https://sourceforge.net/p/nuto/wiki/MUMPS%20Solver%20package/


## How to use ##

In your source directory of MUMPs, running the following command will
add the source files needed by CMake:
```shell
git archive --remote=https://git-xen.lmgc.univ-montp2.fr/mozul/MUMPS_cmake.git master | tar -x -C .
```

This will add the filetree needed by CMake to do the configuration.
Then create and go to a build directory and configure the project:
```shell
mkdir build
cd build
cmake ..
```

## Options ##

You can parameter the building by setting some options.
The list of available options is (default value in parenthesis):
* `CMAKE_Fortran_COMPILER`  : to select the compiler suite to use
* `PARA_LEVEL`         (seq) : level of parallelization ('seq', 'openMP' or 'mpi')
* `BUILD_SHARED_LIBS` (ON)  : build shared libraries
* `ENABLE_METIS`      (OFF) : to use (Par)Metis library
* `ENABLE_SCOTCH`     (OFF) : to use (PT-)Scotch library
* `ENABLE_REAL`       (ON)  : support for real numbers
* `ENABLE_COMPLEX`    (OFF) : support for complex numbers
* `ENABLE_SINGLE_PRECISION` (OFF): support for single precision
* `ENABLE_DOUBLE_PRECISION` (ON) : support for double precision

The option can be set by using a Graphical User Interface (like `cmake-gui`),
or within the terminal by using `ccmake` executable to select them or specifying
them when configuring:
running:
```shell
cmake . -DOPTION_NAME=VALUE
```

Furthermore there are some variables defined by cmake that allow to
fine tune the building:
* `CMAKE_INSTALL_PREFIX`: to change the default installation path
* `CMAKE_C_FLAGS`       : to add some specific compilation flags to C compiler
* `CMAKE_Fortran_FLAGS` : to add some specific compilation flags to Fortran compiler

## Building ##

Finally once the configuration step done, you
can build, test and install:
```shell
make -j4
make test
make install
```

